#!/usr/bin/env python3
# Semantic toe
# Description:
# Small script that prints a random game
# where both players follows a peculiar strategy.

import numpy as np
from itertools import product


def swap_numbers_for_symbols(number):
    code = {0: ' ', 3: 'X', 5: 'O'}
    return code.get(number, None)


def pretty_print_board(board, available_moves):
    print(
        "\n"
        " %s | %s | %s \n"
        "---+---+---\n"
        " %s | %s | %s \n"
        "---+---+---\n"
        " %s | %s | %s \n" % (
            swap_numbers_for_symbols(board[0, 0]),
            swap_numbers_for_symbols(board[0, 1]),
            swap_numbers_for_symbols(board[0, 2]),
            swap_numbers_for_symbols(board[1, 0]),
            swap_numbers_for_symbols(board[1, 1]),
            swap_numbers_for_symbols(board[1, 2]),
            swap_numbers_for_symbols(board[2, 0]),
            swap_numbers_for_symbols(board[2, 1]),
            swap_numbers_for_symbols(board[2, 2])
        )
    )


def intermediate_printing(board, available_moves):
    pretty_print_board(board, available_moves)
    return board, available_moves


def init_empty_board():
    return np.zeros((3, 3)), list(product([0, 1, 2], [0, 1, 2]))


def move(board, available_moves, coord, player):
    board[coord[0], coord[1]] = player
    available_moves.remove(coord)
    return board, available_moves


def first_movement(board, available_moves):
    return move(*move(board, available_moves, available_moves[np.random.choice(len(available_moves))], 3), available_moves[np.random.choice(len(available_moves))], 5)


def next_movement(board, available_moves, player):
    candidate_rows = [n_row for n_row in range(board.shape[0]) if not sum(board[n_row, :]) == player]
    candidate_cols = [n_col for n_col in range(board.shape[1]) if not sum(board[:, n_col]) == player]
    candidate_diag = np.trace(board) != player
    candidate_back_diag = np.trace(np.fliplr(board)) != player

    candidate_coords = list(product(candidate_rows, candidate_cols))

    for bad_coord in zip([0, 1, 2], [0, 1, 2]):
        if not candidate_diag and bad_coord in candidate_coords:
            candidate_coords.remove(bad_coord)

    for bad_coord in zip([0, 1, 2], [2, 1, 0]):
        if not candidate_back_diag and bad_coord in candidate_coords:
            candidate_coords.remove(bad_coord)
    
    print("Available moves: " + repr(available_moves))
    print("Candidates: " + repr(candidate_coords))
    
    for coord in candidate_coords:
        if board[coord[0], coord[1]] == 0:
            return move(board, available_moves, coord, player)
        continue

    print("\nI don't want to continue playing because the situation will lead to an aggressive movement.\n"
          "And i don't want to win, nor to loose.\n"
          "I'm a dissident.\n")
    exit(1)

# Main program
pretty_print_board(*next_movement(*intermediate_printing(*next_movement(*intermediate_printing(*next_movement(*intermediate_printing(*next_movement(*intermediate_printing(*next_movement(*intermediate_printing(*next_movement(*intermediate_printing(*next_movement(*intermediate_printing(*first_movement(*intermediate_printing(*init_empty_board()))), 3)), 5)), 3)) ,5)), 3)), 5)), 3))

