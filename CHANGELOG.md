# CHANGELOG

This changelog adheres to [Semantic Versioning](http://semver.org).

## 1.1.3

* Format fixes.


## 1.1.2

* Add description of simulation script.


## 1.0.2

* Linguistic review from reverso.net


## 1.0.1

* Asciidoc format fixes for gitlab rendering.


## 1.0.0

* First complete draft.


## 0.1.0

* First skeleton.
